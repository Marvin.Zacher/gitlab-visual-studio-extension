﻿using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Autofac;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    [Ignore("Dericated")]
    public class LsProcessManagerGolangTests : TestBase
    {
        [SetUp]
        public void Setup()
        {
            CreateBuilder()
                .RegisterLogging()
                .RegisterCodeSuggestions()
                .RegisterSettings()
                .RegisterStatus()
                .RegisterLanguageServer(LsImpl.Golang)
                .BuildScope();

            LsCommon.KillLanguageServer();
        }

        [Test]
        public async Task StartAndStopAsync()
        {
            Assert.IsFalse(
                LsCommon.IsLanguageServerRunning(out _), 
                "Expected that no language servers would exist before test is run");

            var ret = _scope.Resolve<ILsProcessManager>().StartLanguageServer(
                TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                out var firstPort);

            Assert.IsTrue(ret, "Expected StartLanguageServer to return true.");
            Assert.IsTrue(
                LsCommon.IsLanguageServerRunning(out var count), 
                "Expected language server to have been started.");
            Assert.AreEqual(1, count, $"Expected 1 process, but got {count} processes.");

            try
            {
                ret = _scope.Resolve<ILsProcessManager>().StartLanguageServer(
                    TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                    out var secondPort);

                Assert.IsFalse(ret, "Expected second StartLanguageServer call to return false.");
                Assert.AreEqual(firstPort, secondPort, $"Expected firstPort ({firstPort}) and secondPort ({secondPort}) to be the same.");
                LsCommon.IsLanguageServerRunning(out count);
                Assert.AreEqual(1, count, $"Expected 1 process after second StartLanguageServer, but got {count} processes.");
            }
            finally
            {
                ret = await _scope.Resolve<ILsProcessManager>().StopLanguageServerAsync(TestData.SolutionPath);

                Assert.IsTrue(ret, "Expected StopLanguageServerAsync to return true.");
                Assert.IsFalse(
                    LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out _),
                    "Expected the language server process to have been stopped.");
            }

            // Make sure starting it again works as expected

            // Hold the firstPort so we can verify we get a different port
            LsCommon.HoldPort(firstPort);
            try
            {
                ret = _scope.Resolve<ILsProcessManager>().StartLanguageServer(
                    TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                    out var thirdPort);

                Assert.IsTrue(ret, "Expected StartLanguageServer to return true.");
                Assert.IsTrue(
                    LsCommon.IsLanguageServerRunning(out count),
                    "Expected language server to have been started.");
                Assert.AreEqual(1, count, $"Expected 1 process, but got {count} processes.");
                Assert.AreNotEqual(firstPort, thirdPort,
                    $"Expected firstPort ({firstPort}) and thirdPort ({thirdPort}) to be different. firstPort was held and could not be used by a new LS.");

                try
                {
                    ret = _scope.Resolve<ILsProcessManager>().StartLanguageServer(
                        TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                        out var fourthPort);

                    Assert.IsFalse(ret, "Expected second StartLanguageServer call to return false.");
                    Assert.AreEqual(thirdPort, fourthPort, $"Expected thirdPort ({thirdPort}) and fourthPort ({fourthPort}) to be the same.");
                    LsCommon.IsLanguageServerRunning(out count);
                    Assert.AreEqual(1, count, $"Expected 1 process after second StartLanguageServer, but got {count} processes.");
                }
                finally
                {
                    ret = await _scope.Resolve<ILsProcessManager>().StopLanguageServerAsync(TestData.SolutionPath);
                    Assert.IsTrue(ret, "Expected StopLanguageServerAsync to return true.");
                    Assert.IsFalse(
                        LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out _),
                        "Expected the language server process to have been stopped.");
                }
            }
            finally
            {
                LsCommon.ReleasePort(firstPort);
            }
        }
    }
}
