﻿using Autofac;
using NUnit.Framework;
using System;
using GitLab.Extension.SettingsUtil;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Text;
using Serilog;

namespace GitLab.Extension.Tests
{
    [TestFixture]
    public class SettingsTests : TestBase
    {
        private bool _onSettingsChangedEventCalled = false;
        private Settings.SettingsEventArgs _onSettingsChangedEventArgs;
        private ISettingsProtect _settingsProtect;
        private ISettings _settings;

        [SetUp]
        public void Setup()
        {
            _onSettingsChangedEventCalled = false;
            _onSettingsChangedEventArgs = null;

            CreateBuilder()
                .RegisterLogging()
                .RegisterSettings()
                .BuildScope();

            _settings = _scope.Resolve<ISettings>();
            _settingsProtect = _scope.Resolve<ISettingsProtect>();
        }

        [TearDown]
        public void Teardown()
        {
            TestData.ResetSettings(_settings);
        }

        #region Helper methods

        public class TestNullProtect : ISettingsProtect
        {
            public string Protect(string data)
            {
                return data;
            }

            public string Unprotect(string protectedData)
            {
                return protectedData;
            }
        }

        private void OnSettingsChangedEvent(object sender, EventArgs e)
        {
            _onSettingsChangedEventArgs = e as Settings.SettingsEventArgs;
            _onSettingsChangedEventCalled = true;
        }

        private static bool DoesRegistryKeyExist(string registryKey)
        {
            var key = Registry.CurrentUser.OpenSubKey($"Software\\{registryKey}");
            return key != null;
        }

        private string Protect(string data)
        {
            return _settingsProtect.Protect(data);
        }
        
        #endregion


        [Test]
        public void ConfiguredTest()
        {
            _settings.GitLabAccessToken = string.Empty;
            Assert.IsFalse(_settings.Configured, "Expected Configured to return false when access token isn't set");
            _settings.GitLabAccessToken = "glpat-abcdef";
            Assert.IsTrue(_settings.Configured, "Expected Configured to return true when access token is set");
        }

        [Test]
        public void SettingsChangedEventTest()
        {
            _settings.SettingsChangedEvent += OnSettingsChangedEvent;
            try
            {
                _settings.GitLabAccessToken = "glpat-abcdef";
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.GitLabAccessTokenKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.GitLabAccessTokenKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");

                _onSettingsChangedEventCalled = false;

                _settings.IsCodeSuggestionsEnabled = false;
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.IsCodeSuggestionsEnabledKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.IsCodeSuggestionsEnabledKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");

                _onSettingsChangedEventCalled = false;

                _settings.GitLabUrl = "https://abc.com";
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.GitLabUrlKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.GitLabUrlKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");
            }
            finally
            {
                _settings.SettingsChangedEvent -= OnSettingsChangedEvent;
            }
        }

        [Test]
        public void ConvertPoCSettingsTest()
        {
            if(DoesRegistryKeyExist(Settings.ApplicationName))
                Registry.CurrentUser.DeleteSubKey($"Software\\{Settings.ApplicationName}");

            var key = Registry.CurrentUser.CreateSubKey($"Software\\{Settings.ApplicationNamePoC}", true);
            key.SetValue(Settings.GitLabAccessTokenKey,
                Protect(TestData.CodeSuggestionsToken), RegistryValueKind.String);

            Assert.IsTrue(DoesRegistryKeyExist(Settings.ApplicationNamePoC),
                "Expect PoC registry key to exist");
            Assert.IsFalse(DoesRegistryKeyExist(Settings.ApplicationName),
                "Expect settings registry key to not exist");

            var settings = _settings as Settings;
            settings.Storage.Load(settings);

            Assert.IsFalse(DoesRegistryKeyExist(Settings.ApplicationNamePoC),
                "Expecte PoC registry key to have been deleted");
            Assert.IsTrue(DoesRegistryKeyExist(Settings.ApplicationName),
                "Expect settings registry key to exist");
        }
    }
}
