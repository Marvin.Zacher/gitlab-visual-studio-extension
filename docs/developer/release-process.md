# Release process

We release irregularly: when there is a fix, or user-facing feature, that provides value to the users.
See the CHANGELOG for [release dates](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/CHANGELOG.md).

Only those with the Owner or Maintainer role can release this extension.

## Perform release

To release a new version of the extension:

1. Optional. Do a quick test of the extension in your local environment. At this stage, you are verifying only that there is no complete failure of the extension.
1. Merge the latest change.
1. After the latest pipeline on the `main` branch is green, on the `release` job, select **Play**.
1. After the new version of the extension shows up in the
   [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio), update your extension locally.
1. Do a quick test of the extension in your local environment. At this stage, you are verifying only that there is no complete failure of the extension.
1. Add a message to our `#f_visual_studio_extension` Slack channel that a new version has been released.

## Breaking or major changes

Increment `RELEASE_VERSION` in [`vsix.properties`](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/vsix.properties).

This can be done either in advance of (or during) major or breaking changes.

## Community contributions

After release, if new community contributions were added, manually add attribution to `CHANGELOG.md`.

## Access tokens for Marketplaces

_This section applies once a year when the GitLab and Microsoft Visual Studio Marketplace tokens automatically expire._

Our CI/CD jobs use:

- **The GitLab access token** for tagging and releasing.
- **The Microsoft Visual Studio marketplace token** for publishing the extension to the Visual Studio Marketplace.

### How to generate tokens

#### GitLab Personal Access Token

1. Sign in to `gitlab.com` with the credentials from the `Visual Studio Extension` 1Password Vault.
1. Generate a new access token with `api` scope.
1. Update the corresponding
   [CI/CD variable](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/settings/ci_cd).

#### Microsoft Visual Studio Marketplace

1. Sign in to [Microsoft Visual Studio Marketplace](https://marketplace.visualstudio.com/vs) with `Microsoft`
   credentials from the `Visual Studio Extension` 1Password Vault.
1. Go to **GitLab Visual Studio (`visualstudio@gitlab.com`)** and then **Actions > Manage security**.
1. You can either:
   - Extend the expiration date of an existing token. **This solution is the best one when you receive token expiration email.**
   - Generate a new token. It only needs **Marketplace - publish**.
