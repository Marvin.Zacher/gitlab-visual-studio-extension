# Changelog

## 0.15.0 (2023-08-29)

No changes.

## 0.14.0 (2023-08-22)

### changed (2 changes)

- [Switch to TypeScript language server](gitlab-org/editor-extensions/gitlab-visual-studio-extension@69d422cd299e52b5f43f8fa3ed7821107121f4b9) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!52))
- [Upgrade to TypeScript Lang Server v0.0.7](gitlab-org/editor-extensions/gitlab-visual-studio-extension@67be3655665cfebf8c8250966e783c9ba9531909) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!50))

## 0.13.0 (2023-08-21)

No changes.

## 0.12.0 (2023-08-19)

No changes.

## 0.11.0 (2023-08-19)

### added (1 change)

- [Support code suggestions for additional file types](gitlab-org/editor-extensions/gitlab-visual-studio-extension@a7bc443e4171a799142f3cfabb2807f7ed5badd8) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!45))

## 0.10.0 (2023-07-12)

### added (2 changes)

- [Limit suggestions to supported languages](gitlab-org/editor-extensions/gitlab-visual-studio-extension@5bd6e716b6d0a8db9172acf6b69b677716939acb) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!34))
- [Add /docs directory](gitlab-org/editor-extensions/gitlab-visual-studio-extension@4351daf4449de30fde1e1f325d83fd4ae0615afc) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!20))

### fixed (1 change)

- [Better handle empty suggestion](gitlab-org/editor-extensions/gitlab-visual-studio-extension@0a552b8599635b509ed05f58705b748fa9658edb) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!33))

## 0.9.0 (2023-07-05)

### added (1 change)

- [Control logging level through a user setting](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e877a618d05fa4df5b7e12c88a667b2654c0f0d6) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!31))

## 0.8.0 (2023-06-30)

### added (1 change)

- [Adding logging and ability to enable writing log to disk](gitlab-org/editor-extensions/gitlab-visual-studio-extension@2d8dd7756f057bd61de51e8b75a203af3d5c71cd) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!24))

## 0.7.0 (2023-06-28)

### fixed (1 change)

- [Show correct status icon when extension is loaded](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e5d2ac2c6e89e191e7b91a1a4c915e80aa44eae1) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!29))

## 0.6.0 (2023-06-28)

### added (1 change)

- [Make extension public, also tweak README.md](gitlab-org/editor-extensions/gitlab-visual-studio-extension@125e7212b1e96f378ccccb9bd7a3cbc20a68f363) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!28))

## 0.5.0 (2023-06-28)

### fixed (1 change)

- [Pull latest code before building](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e7f74927aca476546fb94b1a6f015949fddd1c0b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!27))

## 0.4.0 (2023-06-28)

### fixed (1 change)

- [Add a `git pull` before publishing VSIX](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e55a1fb84f312531340affd7144a85080d828503) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!26))

## 0.3.0 (2023-06-28)

### added (1 change)

- [Integrate latest language server iteration](gitlab-org/editor-extensions/gitlab-visual-studio-extension@765f58296c6ff2ddf81601031c219be2e751d9ba) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!21))

### changed (1 change)

- [Re-arrange documentation](gitlab-org/editor-extensions/gitlab-visual-studio-extension@a74794530c87534e921d8ec50d2b53391596f96b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!19))

## 0.2.0 (2023-06-22)

No changes.

## 0.1.0 (2023-06-22)

### added (1 change)

- [Add output/buildtag file](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e3ad6661962023498db90b7340f24c52e313476e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!16))

## 0.0.1 (2023-06-22)

### added (1 change)

- [Add output/buildtag file](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e3ad6661962023498db90b7340f24c52e313476e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!16))
