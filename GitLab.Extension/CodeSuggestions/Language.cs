﻿using System.Collections.Generic;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// A language supported by this extension.
    /// This allows:
    ///     - Enabling/disabling based on ContentType
    ///     - Tracking features supported by language as the extension grows.
    /// </summary>
    public struct Language
    {
        public string Name;
        public string ContentType;
        public IReadOnlyList<string> Extensions;
        public bool FeatureCodeSuggestions;

        public Language(string name, string contentType) :
            this(name, contentType, new string[0])
        {
        }

        public Language(string name, string contentType, IReadOnlyList<string> extensions)
        {
            Name = name;
            ContentType = contentType.ToLower();
            Extensions = extensions;
            FeatureCodeSuggestions = true;
        }
    }
}
