﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// Defines all the supported languages and provides
    /// helper methods for determining if a feature can
    /// be provided to a specific language.
    /// </summary>
    public class LanguageManager : ILanguageManager
    {
        private IReadOnlyList<Language> _languages;

        public LanguageManager()
        {
            _languages = new Language[]
            {
                new Language("C", "c"),
                new Language("C++", "cpp"),
                new Language("C#", "csharp"),
                new Language("CUDA C++", "cuda-cpp"),
                new Language("Golang", "go", new [] {"go"}),
                new Language("Java", "java", new [] {"java"}),
                new Language("JavaScript", "javascript"),
                new Language("JavaScript React", "javascriptreact"),
                new Language("Kotlin", "kotlin", new [] { "kt" }),
                new Language("Objective-C", "objective-c"),
                new Language("Objective-C++", "objective-cpp"),
                new Language("PHP", "php"),
                new Language("Python", "python"),
                new Language("Ruby", "ruby"),
                new Language("Rust", "rust", new [] {"rs"}),
                new Language("SQL", "sql", new [] {"sql"}),
                new Language("Scala", "scala"),
                new Language("Swift", "swift", new [] {"swift" }),
                new Language("TypeScript", "typescript"),
                new Language("TypeScript React", "typescriptreact"),
            };
        }

        public IReadOnlyList<Language> Languages
        {
            get { return _languages; }
            set { _languages = value; }
        }

        public string GetExtensionFromFilename(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
                return "";

            var periodIndex = filename.LastIndexOf('.');
            if (periodIndex == -1 || periodIndex+1 >= filename.Length)
                return "";

            return filename.Substring(periodIndex + 1);
        }

        private string FixContentType(string contentType)
        {
            contentType = contentType.ToLower();

            if (contentType.StartsWith("code++."))
                contentType = contentType.Substring("code++.".Length);

            return contentType;
        }

        public bool CheckFeatureCodeSuggestion(string contentType, string extension)
        {
            contentType = FixContentType(contentType);

            var ret = _languages
                .Any(x => x.FeatureCodeSuggestions == true && 
                     (x.ContentType == contentType || x.Extensions.Contains(extension)));

            return ret;
        }

        public Language GetLanguage(string contentType, string extension)
        {
            contentType = FixContentType(contentType);

            var ret = _languages
                .FirstOrDefault(x => x.ContentType == contentType || x.Extensions.Contains(extension));

            return ret;
        }
    }
}
