﻿using Autofac;
using Autofac.Core;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Threading;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using System;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.TextManager.Interop;

namespace GitLab.Extension.CodeSuggestions
{
    [Export(typeof(GitlabProposalManagerProvider))]
    [Export(typeof(ProposalManagerProviderBase))]
    [Name("GitlabProposalManager")]
    [ContentType("any")]
    [Order(Before = "IntelliCodeCSharpProposalManager")]
    [Order(Before = "Highest Priority")]
    public class GitlabProposalManagerProvider : ProposalManagerProviderBase
    {
        public readonly IViewTagAggregatorFactoryService ViewTagAggregatorFactoryService;
        public readonly JoinableTaskFactory JoinableTaskFactory;
        private readonly ILanguageManager _languageManager;
        private readonly Status.StatusBar _statusBar;

        [ImportingConstructor]
        public GitlabProposalManagerProvider(
          IViewTagAggregatorFactoryService viewTagAggregatorFactoryService,
          [Import("GitLab.Extension.CodeSuggestions.PackageJoinableTaskFactory")] JoinableTaskFactory joinableTaskFactory)
        {
            ViewTagAggregatorFactoryService = viewTagAggregatorFactoryService;
            JoinableTaskFactory = joinableTaskFactory;

            _languageManager = DependencyInjection.Instance.Scope.Resolve<ILanguageManager>();
            _statusBar = DependencyInjection.Instance.Scope.Resolve<Status.StatusBar>();
        }

        public override async Task<ProposalManagerBase> GetProposalManagerAsync(ITextView view, CancellationToken cancel)
        {
            await JoinableTaskFactory.SwitchToMainThreadAsync(cancel);

            try
            {
                Logging.ConfigureLogging(view as System.Windows.UIElement);
                _statusBar.InitializeDisplay(view as System.Windows.UIElement);

                if (!_languageManager.CheckFeatureCodeSuggestion(
                    view.TextBuffer.ContentType.DisplayName.ToLower(),
                    _languageManager.GetExtensionFromFilename(GetTextViewFilePath(view))))
                {
                    Log.Debug($"{nameof(GetProposalManagerAsync)}: ContentType ({view.TextBuffer.ContentType.DisplayName}) not supported.");
                    return null;
                }
                else
                {
                    Log.Debug($"{nameof(GetProposalManagerAsync)}: ContentType ({view.TextBuffer.ContentType.DisplayName}) is supported.");
                }

                var ret = DependencyInjection.Instance.Scope.Resolve<GitlabProposalManager>(new Parameter[]
                {
                    new TypedParameter(typeof(ITextView), view),
                    new TypedParameter(typeof(GitlabProposalManagerProvider), this),
                });

                return ret;
            }
            catch(Exception ex)
            {
                Log.Error(ex, "Error Creating instance of GitlabProposalManager: " + ex.Message);
                return null;
            }
        }

        private string GetTextViewFilePath(ITextView view)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            view.TextBuffer.Properties.TryGetProperty(typeof(IVsTextBuffer), out IVsTextBuffer bufferAdapter);
            var persistFileFormat = bufferAdapter as IPersistFileFormat;

            if (persistFileFormat == null)
                return null;

            persistFileFormat.GetCurFile(out var filepath, out _);

            if (string.IsNullOrEmpty(filepath))
                return null;

            return filepath;
        }
    }
}
