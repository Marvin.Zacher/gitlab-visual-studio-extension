﻿using Autofac;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Status;
using AutofacSerilogIntegration;

namespace GitLab.Extension
{
    public class DependencyInjection
    {
        private static DependencyInjection _instance = null;
        private static ISettings _settings = null;

        public static DependencyInjection Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DependencyInjection();

                return _instance;
            }
        }

        public ILifetimeScope Scope { get; }

        private DependencyInjection()
        {
            var container = RegisterComponents();
            Scope = container.BeginLifetimeScope();

            _settings = Scope.Resolve<ISettings>();
        }

        private IContainer RegisterComponents()
        {
            var builder = new ContainerBuilder();

            // CodeSuggestions

            builder.RegisterType<GitlabProposalSource>();
            builder.RegisterType<GitlabProposalManager>();
            builder.RegisterType<LanguageManager>()
                .As<ILanguageManager>()
                .SingleInstance();

            // LanguageServer

            builder.RegisterType<LsClientManager>()
                .As<ILsClientManager>()
                .SingleInstance();
            builder.RegisterType<LsClientTs>()
                .As<ILsClient>();
            builder.RegisterType<LsProcessManagerTs>()
                .As<ILsProcessManager>()
                .SingleInstance();

            // Settings

            builder.RegisterType<Settings>()
                .As<ISettings>()
                .SingleInstance();
            builder.RegisterType<RegistryStorage>()
                .As<ISettingsStorage>()
                .SingleInstance();
            builder.RegisterType<ProtectImpl>()
                .As<ISettingsProtect>()
                .SingleInstance();

            // Status

            builder.RegisterType<Status.StatusBar>()
                .SingleInstance();
            builder.RegisterType<Status.ExtensionStatusControl>()
                .As<IStatusControl>()
                .SingleInstance();

            // Logging

            builder.RegisterLogger();

            return builder.Build();
        }
    }
}
