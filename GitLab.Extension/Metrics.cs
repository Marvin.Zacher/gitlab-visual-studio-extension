﻿using System.Diagnostics.Metrics;

namespace GitLab.Extension
{
    public static class Metrics
    {
        public const string ServiceName = "GitLab.Extension";
        public static Meter Meter = new Meter(ServiceName);
        public static Counter<long> CodeSuggestionsDisplayed = Meter.CreateCounter<long>("extension.codesuggestions_displayed");
        public static Counter<long> CodeSuggestionsAccepted = Meter.CreateCounter<long>("extension.codesuggestions_accepted");
    }
}
