﻿using System;
using System.Threading.Tasks;
using StreamJsonRpc;
using System.Diagnostics;
using GitLab.Extension.LanguageServer.Models;
using System.Reflection;
using GitLab.Extension.SettingsUtil;
using System.Runtime.InteropServices;
using Serilog;
using System.IO;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Code suggestions language server client.
    /// </summary>
    /// <remarks>
    /// Once the client is connected to a language server,
    /// it will automatically reconnect using an exponential backoff
    /// if the connection is lost.
    /// 
    /// Reconnecting means stopping/starting the language server
    /// in addition to our rpc connection.
    /// </remarks>
    public class LsClientTs : LsClientBase, ILsClient
    {
        private Stream _lsInput;
        private Stream _lsOutput;

        /// <summary>
        /// DO NOT INSTANTIATE DIRECTLY, USE LsClientManager!!!
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        public LsClientTs(ISettings settings, ILsProcessManager lsProcessManager, 
            LsClientSolution solution, ILogger logger)
            : base(settings, lsProcessManager, solution, logger)
        {
            _logger.Debug($"{nameof(LsClientTs)}({{SolutionName}}, {{SolutionPath}}",
                solution.Name, solution.Path);
        }

        /// <summary>
        /// Start language server
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns language server port, or -1 on error.</returns>
        protected override async Task<int> StartLanguageServerAsync(string solutionPath)
        {
            _logger.Debug($"{nameof(StartLanguageServerAsync)}({{SolutionPath}})",
                solutionPath);

            if (_lsInput != null)
            {
                _logger.Debug($"{nameof(StartLanguageServerAsync)} _lsInput and _lsOutput already set");
                return 0;
            }

            _lsInput = _lsOutput = null;

            await Task.Run(() =>
            {
                try
                {
                    _logger.Debug($"{nameof(StartLanguageServerAsync)} Setting and starting _lsProcessManager");
                    _lsProcessManager.StartLanguageServerStdio(
                        solutionPath,
                        _settings.GitLabUrl,
                        _settings.GitLabAccessToken,
                        out _lsInput,
                        out _lsOutput);
                }
                catch(Exception ex)
                {
                    _logger.Debug(ex, $"{nameof(StartLanguageServerAsync)} exception");
                    if(ex.InnerException != null)
                        _logger.Debug(ex.InnerException, $"LsClient.{nameof(StartLanguageServerAsync)} innerException");
                }
            });

            _settings.SettingsChangedEvent += SettingsChangedEvent;

            return 0;
        }

        /// <summary>
        /// Called when the settings change to restart
        /// our language server with latest settings.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsChangedEvent(object sender, EventArgs e)
        {
            if (_disposed)
                return;

            _logger.Debug($"{nameof(SettingsChangedEvent)}");

#pragma warning disable VSTHRD002 // Avoid problematic synchronous waits
            SendDidChangeConfigurationNotificationAsync(
                _settings.GitLabAccessToken,
                _settings.GitLabUrl).GetAwaiter().GetResult();
#pragma warning restore VSTHRD002 // Avoid problematic synchronous waits
        }

        /// <summary>
        /// Cleanup resources before restarting
        /// </summary>
        private async Task RestartCleanupAsync()
        {
            _lsInput = _lsOutput = null;

            if (_rpc != null)
                _rpc.Disconnected -= rpc_Disconnected;

            _rpc = null;

            await _lsProcessManager.StopLanguageServerAsync(_solutionPath);
        }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <param name="reconnect">Reconnect to the language server.</param>
        /// <returns>True on success, false on failure</returns>
        protected override async Task<bool> ConnectAsync(bool reconnect = false)
        {
            try
            {
                if (_disposed)
                    throw new ObjectDisposedException(nameof(LsClientTs));

                if (_disposing)
                    return false;

                _logger.Debug($"{nameof(ConnectAsync)}({{Reconnect}}): Name: {{SolutionName}}, Path: {{SolutionPath}}",
                    reconnect, _solutionName, _solutionPath);

                // Make sure we have a valid configuration
                if (!_settings.Configured)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} Settings.Instance.Configured == false, returning false");
                    return false;
                }

                if (!reconnect && _lsInput != null)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} !reconnect && _lsInput != null, returning false");
                    return false;
                }

                // Use our backoff helper to make sure we don't
                // keep restarting in a tight loop.
                // There is no delay on first call
                if (!_backoffHelper.ShouldRetry)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} !_backoffHelper.ShouldRetry, returning false");
                    return false;
                }
                await _backoffHelper.DelayAsync();

                // If we are reconnecting cleanup existing resources
                if (reconnect)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} await RestartCleanupAsync");
                    await RestartCleanupAsync();
                }

                // Start a license server instance if we haven't already
                await StartLanguageServerAsync(_solutionPath);
                if (_lsInput == null || _lsOutput == null)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} _lsInput = null || _lsOutput = null, returning false");
                    return false;
                }

                _logger.Debug($"{nameof(ConnectAsync)} Connecting via stdio");

                // Attach to the network stream for RPC
                _logger.Debug($"{nameof(ConnectAsync)} JsonRpc.Attach");
                _rpc = JsonRpc.Attach(_lsInput, _lsOutput, new LsClientRpc());
                _rpc.Disconnected += rpc_Disconnected;

                // Send the LSP 'initialize' message
                _logger.Debug($"{nameof(ConnectAsync)} SendInitializeAsync");
                var initializationResult = await SendInitializeAsync(_solutionName, _solutionPath);

                _rpcConnected = true;

                if (!await SendDidChangeConfigurationNotificationAsync(_settings.GitLabAccessToken, _settings.GitLabUrl))
                {
                    _logger.Debug($"{nameof(ConnectAsync)} SendDidChangeConfigurationNotificationAsync failed");
                    return false;
                }

                return true;
            }
            catch(Exception ex)
            {
                // TODO - Log errors/display to user somehow

                _logger.Warning(ex, $"{nameof(ConnectAsync)} exception");
                if (ex.InnerException != null)
                    _logger.Warning(ex.InnerException, $"{nameof(ConnectAsync)} inner exception");

                return false;
            }
        }

        /// <summary>
        /// Send the workspace/didChangeConfiguration notification message.
        /// This message is used to set and update configuration values
        /// such as the GitLab token.
        /// </summary>
        /// <param name="gitlabToken">GitLab personal access token</param>
        /// <param name="gitlabBaseUrl">GitLab server url</param>
        /// <returns></returns>
        private async Task<bool> SendDidChangeConfigurationNotificationAsync(string gitlabToken, string gitlabBaseUrl)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendDidChangeConfigurationNotificationAsync)}(***, {gitlabBaseUrl})");

                var didChangeConfig = new
                {
                    settings = new
                    {
                        token = gitlabToken,
                        baseUrl = gitlabBaseUrl,
                    }
                };

                await _rpc?.NotifyWithParameterObjectAsync("workspace/didChangeConfiguration", didChangeConfig);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendDidChangeConfigurationNotificationAsync)} exception, returning false");
                return false;
            }

        }

        protected override async Task<object> SendInitializeAsync(string solutionName, string solutionPath)
        {
            try
            {
                _logger.Debug($"{nameof(SendInitializeAsync)}({{SolutionName}}, {{SolutionPath}})",
                    solutionName, solutionPath);

                var clientCapabilities = new ClientCapabilities()
                {
                    textDocument = new ClientCapabilities.TextDocumentClientCapabilities()
                    {
                        completion = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities()
                        {
                            competionItem = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities.CompletionItemClientCapabilities()
                            {
                                documentationFormat = MarkupKind.PlainText,
                                insertReplaceSupport = false,
                            },
                            completionItemKind = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities.CompletionItemKindClientCapabilities()
                            {
                                valueSet = new CompletionItemKind[] { CompletionItemKind.Text }
                            },
                            contextSupport = false,
                            insertTextMode = InsertTextMode.AdjustIndentation,
                        },
                    },
                };

                var initializeParams = new InitializeParams
                {
                    processId = Process.GetCurrentProcess().Id,
                    capabilities = clientCapabilities,
                    clientInfo = new InitializeParams.ClientInfo()
                    {
                        name = "gl-visual-studio-extension",
                        version = $"{Assembly.GetExecutingAssembly().GetName().Version}; arch:{RuntimeInformation.OSArchitecture}; vs:{VSVersion.FullVersion}; os:{VSVersion.OSVersion}",
                    },
                    workspaceFolders = new WorkspaceFolder[]
                    {
                        new WorkspaceFolder()
                        {
                            name = solutionName,
                            // Uri will automatically convert a file path (c:\...) to
                            // a uri (file:///c:/...)
                            uri = FilePathToUri(solutionPath),
                        },
                    },
                    // Optional configuration to control secret redaction (Defaults to on)
                    //initializationOptions = new
                    //{
                    //    codeCompletion = new {
                    //        enableSecretRedaction = false,
                    //    },
                    //}
                };

                var initializeResult = await _rpc?.InvokeWithParameterObjectAsync<dynamic>(
                    "initialize", initializeParams);

                // Read TextDocumentSyncKind
                var jsonResult = initializeResult as JObject;
                var serverCapabilities = (JObject)jsonResult["capabilities"];
                TextDocumentSyncKind = (TextDocumentSyncKind) serverCapabilities.Value<int>("textDocumentSync");
                _logger.Debug($"{nameof(LsClientTs)}.{nameof(SendInitializeAsync)} TextDocumentSyncKind: {TextDocumentSyncKind}");

                return initializeResult;
            }
            catch(Exception ex)
            {
                _logger.Warning(ex, $"LsClient.{nameof(SendInitializeAsync)} exception");
                throw;
            }
        }

        /// <summary>
        /// Common dispose items that are not async. Called
        /// by both DisposeAsync and Dispose.
        /// </summary>
        private void CommonDispose()
        {
            _settings.SettingsChangedEvent -= SettingsChangedEvent;
        }

        public async ValueTask DisposeAsync()
        {
            if (_disposed)
                return;
            
            _disposing = true;

            _logger.Debug($"{nameof(DisposeAsync)}");
            CommonDispose();
            await RestartCleanupAsync();

            _disposed = true;

            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposing = true;

            _logger.Debug($"{nameof(Dispose)}");

            CommonDispose();
#pragma warning disable VSTHRD002 // Avoid problematic synchronous waits
            RestartCleanupAsync().GetAwaiter().GetResult();
#pragma warning restore VSTHRD002 // Avoid problematic synchronous waits

            _disposed = true;

            GC.SuppressFinalize(this);
        }
    }
}
